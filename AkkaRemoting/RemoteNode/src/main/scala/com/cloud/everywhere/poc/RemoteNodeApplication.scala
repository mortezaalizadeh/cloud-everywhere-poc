package com.cloud.everywhere.poc

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import akka.kernel.Bootable

class RemoteNodeApplication extends Bootable {
	val system = ActorSystem("RemoteNodeApp", ConfigFactory.load().getConfig("RemoteSys"))

	def startup = {
		system.actorOf(Props[RemoteActor], name = "remoteActor")
	}

	def shutdown = {
		system.shutdown()
	}
}
