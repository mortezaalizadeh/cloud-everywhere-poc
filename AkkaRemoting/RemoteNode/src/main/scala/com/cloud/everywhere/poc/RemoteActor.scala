import akka.actor._

package com.cloud.everywhere.poc {
	class RemoteActor extends Actor {
		def receive: Receive = {
			case message: String =>
				sender.tell(message + " got something", self)
		}
	}
}
