import akka.actor._
import com.typesafe.config.ConfigFactory

package com.cloud.everywhere.poc {
	object LocalNodeApplication {
		def main(args: Array[String]): Unit = {
			val config = ConfigFactory.load().getConfig("LocalSys")
			val system = ActorSystem("LocalNodeApp", config)

			val clientActor = system.actorOf(Props[LocalActor])
			clientActor ! "Hello"

			Thread.sleep(4000)
			system.shutdown()
		}
	}
}
