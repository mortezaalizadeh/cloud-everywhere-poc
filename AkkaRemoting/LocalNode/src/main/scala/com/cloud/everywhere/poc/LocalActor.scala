import akka.actor._
import akka.util._
import scala.concurrent.Await
import akka.pattern.ask

package com.cloud.everywhere.poc {
	class LocalActor extends Actor with ActorLogging {
		val remoteActor = context.actorSelection("akka.tcp://RemoteNodeApp@ubuntu:2552/user/remoteActor")
		implicit val timeout = Timeout(5)

		def receive: Receive = {
			case message: String =>
				val future = (remoteActor ? message).mapTo[String]
				val result = Await.result(future, timeout.duration)
				log.info("Message received from Server -> {}", result)
		}
	}
}
