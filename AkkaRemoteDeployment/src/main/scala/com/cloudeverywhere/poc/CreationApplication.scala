package com.cloudeverywhere.poc

import akka.kernel.Bootable
import com.typesafe.config.ConfigFactory
import akka.actor._
import scala.util.Random

class CreationApplication extends Bootable {
	val system = ActorSystem("RemoteCreation", ConfigFactory.load.getConfig("RemoteCreation"))
	val remoteActor = system.actorOf(Props[MessageGreetingActor], name = "MessageGreeting")
	val localActor = system.actorOf(Props(classOf[CreationActor], remoteActor), name = "CreationActor")

	def saySomething(op : Operation): Unit =
		localActor ! op

	def startup() {

	}

	def shutdown() {
		system.shutdown()
	}
}

class CreationActor(remoteActor : ActorRef) extends Actor {
	def receive = {
		case op: Operation => remoteActor ! op
		case result: OperationResult => result match {
			case SendMessageResult(msg,who,r) =>
				printf("Greeting Message Result: %s to %s (%s)".format(msg,who,r))
		}
	}
}

object CreationRunner {
	def main(args : Array[String]) {
		val app = new CreationApplication
		printf("Creation Application Started")
		while(true) {
			if(Random.nextInt(100) % 2 == 0)
				app.saySomething(Greeting(app.localActor.toString()))
			else
				app.saySomething(SendMessage(Random.nextInt(1000)+" Hi ", app.localActor.toString()))

			Thread.sleep(500)
		}
	}
}