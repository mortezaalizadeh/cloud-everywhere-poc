package com.cloudeverywhere.poc

import akka.kernel.Bootable
import akka.actor.{Props, Actor, ActorSystem}
import com.typesafe.config.ConfigFactory

class GreetingActor extends Actor {
	def receive = {
		case Greeting(who) =>
			println("Saying hi to %s".format(sender))
			sender ! GreetingResult(sender.toString(), "\nHi "+sender.toString())
	}
}

class GreetingApplication extends Bootable {
	val system  = ActorSystem("GreetingApplication", ConfigFactory.load.getConfig("Greeting"))
	val actor = system.actorOf(Props[GreetingActor], "GreetingActor")

	def startup() {
	}

	def shutdown() {
		system.shutdown()
	}
}

object GreetingRunner {
	def main(args: Array[String]) {
		new GreetingApplication
		println("Started greeting application - waiting for messages")
	}
}