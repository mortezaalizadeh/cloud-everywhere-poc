package com.cloudeverywhere.poc

import akka.kernel.Bootable
import com.typesafe.config.ConfigFactory
import akka.actor.{ActorRef, Props, Actor, ActorSystem, Identify, ReceiveTimeout, ActorIdentity}
import scala.concurrent.duration._
import scala.util.Random

class LookupApplication extends Bootable {
	val system = ActorSystem("LookupApplication", ConfigFactory.load.getConfig("RemoteLookup"))
	val remotePath = "akka.tcp://GreetingApplication@127.0.0.1:2552/user/GreetingActor"
	val actor = system.actorOf(Props(classOf[LookupActor], remotePath), "LookupActor")

	def saySomething(op : Operation): Unit =
		actor ! op

	def startup() {

	}

	def shutdown() {
		system.shutdown()
	}
}

class LookupActor(path: String) extends Actor {
	context.setReceiveTimeout(3.seconds)
	sendIdentifyRequest()

	def sendIdentifyRequest(): Unit = 
		context.actorSelection(path) ! Identify(path)

	def receive = {
		case ActorIdentity(`path`, Some(actor)) =>
			printf("Actor identity %s",path)
			context.setReceiveTimeout(Duration.Undefined)
			context.become(active(actor))
		case ActorIdentity(`path`, None) => println(s"Remote actor not available: $path")
		case ReceiveTimeout 			 => sendIdentifyRequest()
		case _							 => println("Not ready yet")
	}

	def active(actor: ActorRef): Actor.Receive = {
		case op: Operation => actor ! op
		case result: OperationResult => result match {
			case GreetingResult(who,r) =>
				printf("\nGreeting to %s (%s)",who,r)
		}
	}
}

object LookupRunner {
	def main(args : Array[String]) {
		val app = new LookupApplication
		println("Lookup application started")
		while(true) {
			app.saySomething(Greeting("me"))
			Thread.sleep(Random.nextInt(1000)+100)
		}
	}
}