package com.cloudeverywhere.poc

import akka.actor.{Actor, ActorRef}

trait Operation

case class Greeting(who : String) extends Operation

case class SendMessage(msg : String, who : String) extends Operation

trait OperationResult

case class GreetingResult(who : String, result : String) extends OperationResult

case class SendMessageResult(msg : String, who : String, result : String) extends OperationResult

class MessageGreetingActor extends Actor {
	def receive = {
		case SendMessage(msg,who) =>
			println("Sending a message from %s.".format(who))
			sender ! SendMessageResult(msg, who, "%s says %s".format(who,msg))
	}
}