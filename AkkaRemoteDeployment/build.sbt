name:="akka-remote-greeting"

version:="1.0"

scalaVersion:="2.10.3"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
	"com.typesafe.akka" % "akka-actor_2.10" % "2.2.3",
	"com.typesafe.akka" % "akka-kernel_2.10" % "2.2.3",
	"com.typesafe.akka" % "akka-remote_2.10" % "2.2.3"
	)